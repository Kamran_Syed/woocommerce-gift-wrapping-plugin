<?php

/**
 * Plugin Name: WooCommerce Gift Wrapping Plugin
 * Plugin URI: 
 * Description: WooCommerce Gift Wrapping Plugin
 * Version: 1.4
 * Author: Agile Solutions PK
 * Author URI:
 */
 ini_set('display_errors', 0);
 if ( !class_exists('Agile_Wc_Gift_Wrapping')){
	class Agile_Wc_Gift_Wrapping{
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu_options_page'));
			add_action('admin_enqueue_scripts', array(&$this, 'admin_scripts') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action( 'add_meta_boxes', array( &$this, 'add_meta_box' ) );
			add_action( 'save_post', array( &$this, 'save' ) );
			add_shortcode( 'aspk_light', array( &$this, 'light' ) );
			add_filter('woocommerce_add_cart_item_data', array( &$this,'add_cart_item_custom_data_vase'), 10, 2);
			add_filter( 'woocommerce_get_cart_item_from_session', array( &$this, 'get_cart_items_from_session'), 999, 3 );
			add_filter( 'woocommerce_add_order_item_meta', array( &$this, 'save_cart_item_meta'), 10, 3 );
			add_action( 'woocommerce_after_add_to_cart_button', array( &$this, 'aspk_add_cart' ) );
			//add_action( 'woocommerce_after_shop_loop_item_title', array( &$this, 'aspk_add_category_product' ) );
			//add_action( 'woocommerce_before_calculate_totals', array( &$this, 'add_custom_price' ));
			//add_filter( 'woocommerce_cart_item_name', array( &$this,'woocommerce_product_title'),20,2);
			//add_action('wp_footer',array( &$this, 'footer'));
			add_filter( 'wc_add_to_cart_message',array( &$this, 'customtomer_add_to_cart_message' ));
			add_action( 'woocommerce_before_order_itemmeta',array( &$this, 'wc_order_detail_fields'),10,3 ) ;
			add_filter('manage_product_posts_columns' , array( &$this,'add_product_columns'));
			add_action( 'manage_product_posts_custom_column' , array( &$this, 'custom_product_column'), 10, 2 );
			add_action( 'bulk_edit_custom_box', array( &$this,'display_custom_bulkaction_product'), 10, 2 );
			add_action( 'wp_ajax_aspk_save_bulk_edit', array( &$this, 'aspk_save_bulk_edit' ));
			//add_filter('woocommerce_cart_calculate_fees',array( &$this,'change_price'), 10, 2);
			add_action( 'woocommerce_after_single_product_summary', array( &$this, 'woocommerce_output_related_products'), 20);

		}
			
		function woocommerce_output_related_products(){
				
			?>
			<script>
				jQuery( document ).ready(function() {
					jQuery('.upsells  ul  li  .aspk_related_product_hide').hide();
				 });
			</script>
			<?php
		}
		
		function change_price( $cart_object){
			global $woocommerce;
			
			foreach ( $cart_object->cart_contents as $key => $value ) {
				//Get the product data like below
				$quant = $value['quantity'];
				$price = $value['data']->price;
				$wrap_amount = $value['agile_wrap_amount'];
				//check your condition
				if( !empty($wrap_amount) ){
					$value['data']->price = $value['data']->price + $wrap_amount;
					$value['data']->post->post_title = $value['data']->post->post_title .'  ( Gift Wrap Added )';
					
				}
				
			}

			 
			
		}
		
		function url_origin($s, $use_forwarded_host=false){
			$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
			$sp = strtolower($s['SERVER_PROTOCOL']);
			$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
			$port = $s['SERVER_PORT'];
			$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
			$host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
			$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
			return $protocol . '://' . $host;
		}
		
		function full_url($s, $use_forwarded_host=false){
			return $this->url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
		}
					
		function aspk_add_category_product(){
			global  $product,$woocommerce;
			
			$url = $this->full_url($_SERVER);
			$pos = strpos($url, 'product-category');
			if($pos){
				return;
			}else{
				$img = get_option( 'wc_gift_image');
				$cart_items = WC()->cart->get_cart();
				foreach($cart_items as $item){
					$item_pro_id = $item['data']->id;
					$wrap_checkbox = $item['agile_wrap_amount'];
					if($wrap_checkbox){
							if($item_pro_id == $product->id){
							?>
							<script>
								jQuery( document ).ready(function() {
									jQuery('#aspk_wrap_value2145').attr('checked','checked');
									jQuery('#aspk_wrap_value2145').attr('y','1');
									jQuery('#aspk_wrap_text_field').val('<?php echo $item['agile_limitedtextfield']; ?>');
								 });
							</script>
							<?php
							}
					}if(empty($wrap_checkbox)){
					?>
						<script>
							jQuery( document ).ready(function() {
								jQuery('#aspk_wrap_value2145').attr('checked',false);
								jQuery('#aspk_wrap_value2145').attr('y','0');
								jQuery('#aspk_wrap_text_field').val('');
							 });
						</script>
					<?php
					}
				}
				$gift_checbox = get_post_meta( $product->id, '_my_gift_checkbox_data', true );
				$wrap_value = get_post_meta( $product->id, '_my_gift_wrapper_data', true );
				if($wrap_value == '0')	{
					
				}else{
						?>
						<div style="clear:left;margin-top:0.5em;width:15em;" class ="aspk_related_product_hide">
							<div style="clear:left;float:left;" id="aspk_hower_image_show">
								<div style="float:left;clear:left;" ><input style="margin-top:4px;" id="aspk_wrap_value2145" y=0 type="checkbox" name="agile_wrap_amount" value="<?php echo $wrap_value; ?>"></div>
								<div style="float:left;clear:left;margin-top:0.2em">
								<?php
									$w_image = get_option( 'wc_gift_image' );
									$w_text = get_option( 'wc_gift_field');
								?>
									<?php echo get_woocommerce_currency_symbol().sprintf("%01.2f", $wrap_value); ?>
								</div>
								<div style="float:left;margin-left:1em;margin-top:0.2em"><?php echo $w_text; ?></div>
							</div>
							<div style="clear:left;display:none;float:left;position:relative;" id="agile_dialog_box">
								<font size="1">(Maximum characters: 100)<br>
								<textarea placeholder="Add Greeting Here" name="limitedtextfield" type="text" id="aspk_wrap_text_field" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,100);" 
								onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,100);" value="" maxlength="100"></textarea>
								<br>You have <input readonly type="text" name="countdown" size="3" value="100"> characters left.</font>
							</div>
						</div>
						<div id="aspk_gullu_butt_image" style="margin-left: 7em; margin-top: -8em; min-height: 12em; width: 13em;display: inline-block; position: absolute;">
						
						</div>
						<div id="aspk_papu_cat_k_dialog" style="display:none;" title="Add Gift Wrap"><img src="<?php echo $img; ?>" id="gift_pack_image" ></div>
						
					<script language="javascript" type="text/javascript">
						function limitText(limitField, limitCount, limitNum) {
							if (limitField.value.length > limitNum) {
								limitField.value = limitField.value.substring(0, limitNum);
							} else {
								limitCount.value = limitNum - limitField.value.length;
							}
						}
						jQuery("#aspk_wrap_value2145").click(function(){
								var xval = jQuery("#aspk_wrap_value2145").attr("y");
								if(xval == 0){
									jQuery("#aspk_wrap_value2145").attr("y", 1);
									jQuery('#aspk_wrap_value2145').attr('checked','checked');
									jQuery("#agile_dialog_box").show();
									//jQuery( "#aspk_papu_cat_k_dialog" ).dialog();
								}else{
									jQuery("#aspk_wrap_value2145").attr("y", 0);
									jQuery("#agile_dialog_box").hide();
									jQuery("#aspk_wrap_text_field").val('');
									jQuery('#aspk_wrap_value2145').attr('checked',false);
									jQuery( "#aspk_papu_cat_k_dialog" ).hide();
								}
						  });
						  jQuery( document ).ready(function() {
							  jQuery("#aspk_hower_image_show").hover(function(){
									jQuery("#aspk_gullu_butt_image").css("background-image", "url('<?php echo $img; ?>')");
									jQuery("#aspk_gullu_butt_image").css("background-repeat", "no-repeat");
									
									},function(){
										jQuery("#aspk_gullu_butt_image").css("background-image", "url('')");
									});
									
							  var xval = jQuery("#aspk_wrap_value2145").attr("y");
								if(xval == 0){
									jQuery("#agile_dialog_box").hide();
									jQuery('#aspk_wrap_value2145').attr('checked',false);
								}else{
									jQuery("#agile_dialog_box").show();
									jQuery('#aspk_wrap_value2145').attr('checked','checked');
								}
						   });
					</script>
					
						<?php
				}
			}
		}
		

		function aspk_save_bulk_edit(){
			
			$post_ids = ( isset( $_POST[ 'post_ids' ] ) && !empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
		//	$mydata = ( isset( $_POST[ 'bulk_wrap_amount' ] ) && !empty( $_POST[ 'bulk_wrap_amount' ] ) ) ? $_POST[ 'bulk_wrap_amount' ] : NULL;
			$mydata =  $_POST[ 'bulk_wrap_amount' ];
		   // if everything is in order
			if ( !empty( $post_ids ) && is_array( $post_ids ) && !empty( $mydata ) ) {
				foreach( $post_ids as $post_id ) {
					update_post_meta( $post_id, '_my_gift_wrapper_data', $mydata );
				}
			}elseif(empty($mydata)){
				foreach( $post_ids as $post_id ) {
					delete_post_meta( $post_id, '_my_gift_wrapper_data' );//this command delete data in wp_postmeta table
				}
				
			}
		}
		
		function display_custom_bulkaction_product( $columns, $post_type ) {
			switch ( $post_type ) {
				case 'product':

					switch( $columns ) {
						case 'myplugin_textdomain':
						   ?><fieldset class="inline-edit-col-right">
							  <div class="inline-edit-group">
								 <label>
									<span class="title">Gift wraping amount:</span>
									<input type="number" min="0" step="0.1" id="agile_wc_gift" name="bulk_wrap_amount" value="" size="25"/>
								 </label>
							  </div>
							</fieldset>
							<script>
								jQuery( '#bulk_edit' ).live( 'click', function() {
								   // define the bulk edit row
								   var $bulk_row = jQuery( '#bulk-edit' );

								   // get the selected post ids that are being edited
								   var $post_ids = new Array();
								   $bulk_row.find( '#bulk-titles' ).children().each( function() {
									  $post_ids.push( jQuery( this ).attr( 'id' ).replace( /^(ttle)/i, '' ) );
								   });

								   // get the release date
								   var $bulk_wrap_amount = $bulk_row.find( 'input[name="bulk_wrap_amount"]' ).val();

								   // save the data
								   jQuery.ajax({
									  url: ajaxurl, // this is a variable that WordPress has already defined for us
									  type: 'POST',
									  async: false,
									  cache: false,
									  data: {
										 action: 'aspk_save_bulk_edit', // this is the name of our WP AJAX function that we'll set up next
										 post_ids: $post_ids, // and these are the 2 parameters we're passing to our function
									 bulk_wrap_amount: $bulk_wrap_amount
									  }
								   });

								});
							</script>
						   <?php
						break;
					}
				break;

			}
		}
		function custom_product_column( $columns, $post_id ) {
			
			
			
			switch ( $columns ) {
					case 'myplugin_textdomain' :
					$wrap_value = get_post_meta( $post_id, '_my_gift_wrapper_data', true );
					if (!$wrap_value)
						_e( 'No Gift Wrap', 'gift warping amount' );
					else
						echo $wrap_value;
					break;

			} 
		}
		function add_product_columns($columns) {
			
			return array_merge($columns, 
			  array('myplugin_textdomain' => __('Gift Wrapping Amount')));
		}
		function wc_order_detail_fields($item_id, $item, $_product){
			 
		}
	
		function woocommerce_product_title($title, $values){
			$wrap_amount = $values['agile_wrap_amount'];
			if($wrap_amount){
				$title = $title.' ( Gift Wrap Added )';
				return $title;
			}
			return $title;
			
		}
		
		function customtomer_add_to_cart_message(){
			global $woocommerce;
			
			$x = WC()->cart->get_cart();
			foreach($x as $y){
			}
			$wrap_amount = $y['agile_wrap_amount'];
			$product_title = $y['data']->post->post_title;
			if($wrap_amount ){
				$return_to  = get_permalink(woocommerce_get_page_id('cart'));
				$message    = sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('View Cart', 'woocommerce'), __('Gift Wrap  successfully added to your cart.', 'woocommerce') );
				return $message;
			}if(empty($wrap_amount)){
				$return_to  = get_permalink(woocommerce_get_page_id('cart'));
				$message    = sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('View Cart', 'woocommerce'),$product_title .'  successfully added to your cart.', 'woocommerce' );
				return $message;
			} 
		}
		
		function footer(){
			$wrap_amount = $_POST['agile_wrap_amount'];
			$wrap_text = $_POST['limitedtextfield'];
			if($wrap_amount){
			?>
			<script>
				jQuery( document ).ready(function() {
					jQuery('#aspk_wrap_value2145').attr('checked','checked');
					jQuery('#aspk_wrap_text_field').val('<?php echo $wrap_text; ?>');
				 });
			</script>
			<?php
			}
		}
		
		function add_custom_price( $cart_object ) {
			global  $woocommerce;
			
			foreach ( $cart_object->cart_contents as $key => $value ) {
				$wrap_amount = $value['agile_wrap_amount'];
				$line_subtotal = $value['line_subtotal'];
				$p_id = $value['product_id'];
				$quantity = $value['quantity'];
				$product =  get_product($p_id);
				$price = $product->get_price( );
				$real_price = $quantity * $price;
				$aspk_price =$value['data']->price;
				if( !empty($wrap_amount) ){
					if($aspk_price == $real_price || $aspk_price < $real_price){
						$value['data']->price = $value['data']->price + $wrap_amount;
						$value['data']->post->post_title = $value['data']->post->post_title .'  ( Gift Wrap Added )';
					
					}
				}
			}
		}
		
		function aspk_add_cart(){
			global  $product,$woocommerce;
			
			$img = get_option( 'wc_gift_image');
			$cart_items = WC()->cart->get_cart();
			foreach($cart_items as $item){
				$item_pro_id = $item['data']->id;
				$wrap_checkbox = $item['agile_wrap_amount'];
				if($wrap_checkbox){
						if($item_pro_id == $product->id){
						?>
						<script>
							jQuery( document ).ready(function() {
								jQuery('#aspk_wrap_value2145').attr('checked','checked');
								jQuery('#aspk_wrap_value2145').attr('y','1');
								jQuery('#aspk_wrap_text_field').val('<?php echo $item['agile_limitedtextfield']; ?>');
							 });
						</script>
						<?php
						}
				}if(empty($wrap_checkbox)){
				?>
					<script>
						jQuery( document ).ready(function() {
							jQuery('#aspk_wrap_value2145').attr('checked',false);
							jQuery('#aspk_wrap_value2145').attr('y','0');
							jQuery('#aspk_wrap_text_field').val('');
						 });
					</script>
				<?php
				}
			}
			$gift_checbox = get_post_meta( $product->id, '_my_gift_checkbox_data', true );
			$wrap_value = get_post_meta( $product->id, '_my_gift_wrapper_data', true );
			if(empty($wrap_value))	{
				
			}else{
					?>
					<div style="clear:left;margin-top:0.5em;width:15em;"  >
						<div style="clear:left;float:left;" id="aspk_hower_image_show">
							<div style="float:left;clear:left;" ><input style="margin-top:4px;" id="aspk_wrap_value2145" y=0 type="checkbox" name="agile_wrap_amount" value="<?php echo $wrap_value; ?>"></div>
							<div style="float:left;margin-top:0.2em">
							<?php
								$w_image = get_option( 'wc_gift_image' );
								$w_text = get_option( 'wc_gift_field');
							?>
								<?php echo get_woocommerce_currency_symbol().sprintf("%01.2f", $wrap_value); ?>
							</div>
							<div style="float:left;margin-left:1em;margin-top:0.2em"><?php echo $w_text; ?></div>
						</div>
						<div style="clear:left;display:none;float:left;position:relative;" id="agile_dialog_box">
							<div style="clear:left;width:20em;" >(Maximum characters: 100)<br>
							<textarea placeholder="Add Greeting Here" name="limitedtextfield" type="text" id="aspk_wrap_text_field" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,100);" 
							onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,100);" value="" maxlength="100"></textarea>
							<br>You have <input readonly type="text" name="countdown" size="3" value="100"> characters left.</div>
						</div>
					</div>
					<div id="aspk_gullu_butt_image" style="margin-left: 7em; margin-top: -8em; min-height: 12em; width: 13em;display: inline-block; position: absolute;">
					
					</div>
					<div id="aspk_papu_cat_k_dialog" style="display:none;" title="Add Gift Wrap"><img src="<?php echo $img; ?>" id="gift_pack_image" ></div>
					
				<script language="javascript" type="text/javascript">
					function limitText(limitField, limitCount, limitNum) {
						if (limitField.value.length > limitNum) {
							limitField.value = limitField.value.substring(0, limitNum);
						} else {
							limitCount.value = limitNum - limitField.value.length;
						}
					}
					jQuery("#aspk_wrap_value2145").click(function(){
							var xval = jQuery("#aspk_wrap_value2145").attr("y");
							if(xval == 0){
								jQuery("#aspk_wrap_value2145").attr("y", 1);
								jQuery('#aspk_wrap_value2145').attr('checked','checked');
								jQuery("#agile_dialog_box").show();
								//jQuery( "#aspk_papu_cat_k_dialog" ).dialog();
							}else{
								jQuery("#aspk_wrap_value2145").attr("y", 0);
								jQuery("#agile_dialog_box").hide();
								jQuery("#aspk_wrap_text_field").val('');
								jQuery('#aspk_wrap_value2145').attr('checked',false);
								jQuery( "#aspk_papu_cat_k_dialog" ).hide();
							}
					  });
					  jQuery( document ).ready(function() {
						  jQuery("#aspk_hower_image_show").hover(function(){
								jQuery("#aspk_gullu_butt_image").css("background-image", "url('<?php echo $img; ?>')");
								jQuery("#aspk_gullu_butt_image").css("background-repeat", "no-repeat");
								
								},function(){
									jQuery("#aspk_gullu_butt_image").css("background-image", "url('')");
								});
								
						  var xval = jQuery("#aspk_wrap_value2145").attr("y");
							if(xval == 0){
								jQuery("#agile_dialog_box").hide();
								jQuery('#aspk_wrap_value2145').attr('checked',false);
							}else{
								jQuery("#agile_dialog_box").show();
								jQuery('#aspk_wrap_value2145').attr('checked','checked');
							}
					   });
				</script>
				
					<?php
			}
		}
		
		function save_cart_item_meta( $item_id, $values, $cart_item_key){
			$wrap_amount = $values['agile_wrap_amount'];
			$wrap_message = $values['agile_limitedtextfield'];
			wc_add_order_item_meta( $item_id, 'Greeting-Message', $wrap_message);
			wc_add_order_item_meta( $item_id, 'Gift-Wrapping-Amount', $wrap_amount);
			return $values;
		}
 
		
		function add_cart_item_custom_data_vase($cart_item_meta, $product_id){
			 global $woocommerce;
			 
			$cart_item_meta['agile_wrap_amount'] = $_POST['agile_wrap_amount'];
			$cart_item_meta['agile_limitedtextfield'] = $_POST['limitedtextfield'];
			return $cart_item_meta; 
		}
		
		//Get it from the session and add it to the cart variable
		function get_cart_items_from_session( $item, $values, $key ) {
			if ( array_key_exists( 'agile_wrap_amount', $values ) )
				$item[ 'agile_wrap_amount' ] = $values['agile_wrap_amount'];
				$item[ 'agile_limitedtextfield' ] = $values['agile_limitedtextfield'];
				$wrap_amount = $item['agile_wrap_amount'];
				if(!empty($wrap_amount)){
					$item['data']->post->post_title = $item['data']->post->post_title .'  ( Gift Wrap Added )';
					$item['data']->price = $item['data']->price + $wrap_amount ;
					return $item;
				}
				return $item;
		}
		
		public function add_meta_box( $post_type ) {
            $post_types = array('product');     //limit meta box to certain post types

            if ( in_array( $post_type, $post_types )) {
				add_meta_box(
					'some_meta_box_name'
					,__( 'WC Gift Wrapping', 'myplugin_textdomain' )
					,array( $this, 'render_meta_box_content' )//function to html in to your meta box
					,$post_type
					,'advanced'
					,'high'
				);
            }
		}

		public function render_meta_box_content( $post ) {
		
			// Add an nonce field so we can check for it later.
			wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );

			// Use get_post_meta to retrieve an existing value from the database.
			$value = get_post_meta( $post->ID, '_my_gift_wrapper_data', true );
			$gift_checbox = get_post_meta( $post->ID, '_my_gift_checkbox_data', true );

			// Display the form, using the current value.
			echo '<div for="myplugin_new_field">';
			_e( 'Gift Wrap Price:     ', 'myplugin_textdomain' );
			echo '</div> ';
			?>
			<label><input style="display:none;" type="checkbox"<?php if($gift_checbox =='on'){ echo 'checked  y= 1'; }else{ echo ' y=0'; } ?> id="aspk_wc_gift"  name="aspk_checkbox" ></label>
			<label><input type="number" min="0" step="0.01" style="display:block;" id="agile_wc_gift" name="myplugin_new_field" value="<?php echo $value; ?>" size="25" /></label>
			<?php
// here you can put all html you want
		?>
		<script>
			/*  jQuery( document ).ready(function() {
				 var xval = jQuery("#aspk_wc_gift").attr("y");
					if(xval == 0){
						jQuery("#agile_wc_gift").val(0);
					}
					if(xval == 1){
						jQuery("#agile_wc_gift").show();
					}
			  });
			   jQuery("#aspk_wc_gift").click(function(){
					var xval = jQuery("#aspk_wc_gift").attr("y");
					if(xval == 0){
						jQuery("#aspk_wc_gift").attr("y", 1);
						jQuery("#agile_wc_gift").show();
					}else{
						jQuery("#aspk_wc_gift").attr("y", 0);
						jQuery("#agile_wc_gift").hide();
						jQuery("#agile_wc_gift").val(0);
					}
			  }); */
		</script>
		<?php
		}

		public function save( $post_id ) {		
			$mydata = sanitize_text_field( $_POST['myplugin_new_field'] );
			$aspk_chekbox = sanitize_text_field( $_POST['aspk_checkbox'] );
			// Update the meta field.
			/* if(! empty($mydata) && !empty($aspk_chekbox)){
				update_post_meta( $post_id, '_my_gift_wrapper_data', $mydata );//this command update data in wp_postmeta table
				update_post_meta( $post_id, '_my_gift_checkbox_data', $aspk_chekbox );
			}elseif(empty($aspk_chekbox) && (!empty($_POST))){
				delete_post_meta( $post_id, '_my_gift_wrapper_data' );//this command update data in wp_postmeta table
				delete_post_meta( $post_id, '_my_gift_checkbox_data' );
			} */
			if(! empty($mydata)){
				update_post_meta( $post_id, '_my_gift_wrapper_data', $mydata );//this command update data in wp_postmeta table
			}elseif(empty($mydata) && (!empty($_POST))){
				delete_post_meta( $post_id, '_my_gift_wrapper_data' );//this command update data in wp_postmeta table
			}
		}

		
		function install(){
			
		}
		
		function admin_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
		function frontend_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_style('jquery-ui-css',"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css");
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
		function admin_menu_options_page(){
			add_options_page( 'Gift Wrap', 'Gift Wrap', 'manage_options', 'agile_wc_gift_wrap',array( &$this , 'gift_wrap_settings' ));
		}
		
		function gift_wrap_settings(){
			if( isset( $_POST['aspk_set_gift'] ) ){
				$wc_gift_text = $_POST['wc_gift'];				
				if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
				$uploadedfile = $_FILES['aspk_file'];
				$upload_overrides = array( 'test_form' => false );
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				$file_url = $movefile['url'];
				$file_paths = $movefile['file'];
				update_option( 'wc_gift_image', $file_url );
				update_option( 'wc_gift_field', $wc_gift_text );
				?>
			<?php
			}
			$text = get_option( 'wc_gift_field');
			$img = get_option( 'wc_gift_image');
			?>
		<div style="float:left;clear:left;padding:3em;background-color:#FFFFFF;margin-top: 1em;"> 
			<div style="float:left;clear:left;">
				<h3>Gift Wrap Settings </h3>
			</div>
			<div style="float:left;clear:left;">
				<div class = "tw-bs container">	
					<form method = "post" action="" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col-md-12">
								<input type="text" name="wc_gift" value="<?php echo $text; ?>" required />
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12" style="margin-top:1em;">
								<input type="file" name="aspk_file" required  />
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12" style="margin-top:1em;">
								<input type = "submit" name = "aspk_set_gift" class = "btn btn-default"   value = "Save"/> 
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
			<?php
		}
	}
}
new Agile_Wc_Gift_Wrapping();